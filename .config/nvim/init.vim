syntax on

set noerrorbells
set nu
set relativenumber
set tabstop=4 softtabstop=4
set nowrap
set noswapfile
set undodir=~/.vim-undo
set undofile
set incsearch
set noshowmode

set colorcolumn=80
highlight ColorColumn ctermbg=0 guibg=lightgrey

call plug#begin('~/.vim/plugged')

Plug 'dracula/vim'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

Plug 'jiangmiao/auto-pairs'
Plug 'machakann/vim-sandwich'

Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'tpope/vim-sleuth'
Plug 'editorconfig/editorconfig-vim'

Plug 'airblade/vim-gitgutter'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'elmcast/elm-vim'

call plug#end()

autocmd vimenter * colorscheme dracula

let g:ale_fix_on_save = 1
let g:airline_powerline_fonts = 1

let mapleader = ' '

map <c-p> :Files<CR>
map <leader>h :wincmd h<CR>
map <leader>j :wincmd j<CR>
map <leader>k :wincmd k<CR>
map <leader>l :wincmd l<CR>

map <leader>sv :wincmd v<CR>
map <leader>sh :wincmd s<CR>
map <leader>cw :wincmd c<CR>
